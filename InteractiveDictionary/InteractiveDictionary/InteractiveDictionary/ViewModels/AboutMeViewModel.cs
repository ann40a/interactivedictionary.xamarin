﻿using InteractiveDictionary.Services;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace InteractiveDictionary.ViewModels
{
    class AboutMeViewModel
    {
        private static readonly string MyPhoneNumber = "+375291914133";

        public ICommand CallMe => new Command(e =>
        {
            var dialer = DependencyService.Get<IDialer>();
            dialer?.Dial(MyPhoneNumber);
        });
    }
}
