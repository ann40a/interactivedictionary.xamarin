﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using InteractiveDictionary.Models;
using InteractiveDictionary.Services;
using Xamarin.Forms;

namespace InteractiveDictionary.ViewModels
{
    class FindWordByEarGameViewModel : INotifyPropertyChanged
    {
        private static readonly Random rnd = new Random();
        private readonly WordService _wordService = WordService.Instance;
        private readonly GameResultService _gameResultService = GameResultService.Instance;
        private readonly SpeechService _speechService = SpeechService.Instance;
        private readonly INavigation _navigation;
        private Stopwatch _watch;
        private GameResult _result = new GameResult
        {
            GameType = GameType.FindByEar,
        };

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Word> Words { get; set; }
        public Word SelectedWord { get; set; }
        public bool TryAgain { get; set; } = false;

        public FindWordByEarGameViewModel(INavigation navigation)
        {
            _navigation = navigation;
            Init();
        }

        public async Task Init()
        {
            Words = new ObservableCollection<Word>(await _wordService.SelectRandomWords());
            _result.WordsCount = Words.Count;
            await SelectRandomWord();
            _watch = Stopwatch.StartNew();
        }

        public ICommand OnWordSelect => new Command(async (e) => {
            _result.Attempts += 1;

            var word = (Word)e;
            if (word == SelectedWord)
            {
                TryAgain = false;
                Words.Remove(word);
                await SelectRandomWord();
            }
            else
            {
                TryAgain = true;
                await PlaySelectedWord();
            }
        });

        private async Task SelectRandomWord()
        {
            if (Words.Count == 0)
            {
                _watch.Stop();
                _result.ElapsedTime = _watch.ElapsedMilliseconds / 100;
                var page = _navigation.NavigationStack.Last();
                await _navigation.PushAsync(new Views.GameResult(_result));
                _navigation.RemovePage(page);
                await _gameResultService.SaveGameResult(_result);
                return;
            }

            var rndIndex = rnd.Next(0, Words.Count);
            SelectedWord = Words[rndIndex];
            await PlaySelectedWord();
        }

        private async Task PlaySelectedWord()
        {
            await _speechService.SpeakInEnglish(SelectedWord.Value);
        }
    }
}
