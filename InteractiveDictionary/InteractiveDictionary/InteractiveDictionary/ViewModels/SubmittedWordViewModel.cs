﻿using System.ComponentModel;
using System.Windows.Input;
using InteractiveDictionary.Models;
using InteractiveDictionary.Services;
using Xamarin.Forms;

namespace InteractiveDictionary.ViewModels
{
    class SubmittedWordViewModel : INotifyPropertyChanged
    {
        private readonly WordService _wordService = WordService.Instance;
        private readonly SpeechService _speechService = SpeechService.Instance;
        private readonly INavigation _navigation;

        public event PropertyChangedEventHandler PropertyChanged;

        public Word Word { get; set; }
        public bool IsListenEnabled { get; set; } = true;

        public SubmittedWordViewModel(INavigation navigation, Word word)
        {
            _navigation = navigation;
            Word = word;
        }

        public ICommand SaveTranslation => new Command(async () => {
            await _wordService.UpdateWord(Word);
        });

        public ICommand ListenWord => new Command(async () => {
            IsListenEnabled = false;
            await _speechService.SpeakInEnglish(Word.Value);
            IsListenEnabled = true;
        });
    }
}
