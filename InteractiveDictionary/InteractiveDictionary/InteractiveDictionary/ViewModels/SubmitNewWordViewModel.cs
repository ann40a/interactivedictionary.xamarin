﻿using System.ComponentModel;
using System.Windows.Input;
using InteractiveDictionary.Services;
using InteractiveDictionary.Views;
using PropertyChanged;
using Xamarin.Forms;

namespace InteractiveDictionary.ViewModels
{
    class SubmitNewWordViewModel : INotifyPropertyChanged
    {
        private readonly WordService _wordService = WordService.Instance;
        private readonly INavigation _navigation;

        public event PropertyChangedEventHandler PropertyChanged;

        [AlsoNotifyFor(nameof(IsEnabled))]
        public string WordValue { get; set; } = "";

        [AlsoNotifyFor(nameof(IsEnabled))]
        public bool IsFetching { get; set; }

        public bool IsEnabled => !IsFetching && !string.IsNullOrWhiteSpace(WordValue);

        public SubmitNewWordViewModel(INavigation navigation)
        {
            _navigation = navigation;
        }

        public ICommand SubmitWord => new Command(async () =>
        {
            IsFetching = true;
            var word = await _wordService.SubmitWord(WordValue);
            IsFetching = false;

            WordValue = "";
            await _navigation.PushAsync(new SubmittedWord(word));
        });

        public ICommand ShowLatestSubmitted => new Command(async () =>
        {
            WordValue = "";
            await _navigation.PushAsync(new LatestSubmittedWords());
        });
    }
}
