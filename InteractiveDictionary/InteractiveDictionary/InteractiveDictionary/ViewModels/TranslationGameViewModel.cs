﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using InteractiveDictionary.Models;
using InteractiveDictionary.Services;
using Xamarin.Forms;

namespace InteractiveDictionary.ViewModels
{
    class TranslationGameViewModel : INotifyPropertyChanged
    {
        private static readonly string EnToRu = "from EN to RU";
        private static readonly string RuToEn = "from RU to EN";

        private readonly WordService _wordService = WordService.Instance;
        private readonly GameResultService _gameResultService = GameResultService.Instance;
        private readonly INavigation _navigation;
        private List<Word> _words;
        private Stopwatch _watch;
        private GameResult _result = new GameResult
        {
            GameType = GameType.Translation,
        };

        public event PropertyChangedEventHandler PropertyChanged;

        public List<string> TranslationDirections { get; set; } = new List<string> {
            EnToRu,
            RuToEn,
        };
        public string SelectedDirection { get; set; } = EnToRu;
        public bool ShowStartScreen { get; set; } = true;
        public bool ShowGameScreen { get; set; } = false;

        public Word SelectedWord { get; set; }
        public string Value { get; set; }
        public string Translation { get; set; }
        public string ExpectedTranslation { get; set; }

        public bool TryAgain { get; set; } = false;
        public bool ShowTranslation { get; set; } = false;

        public TranslationGameViewModel(INavigation navigation)
        {
            _navigation = navigation;
            Init();
        }

        public async Task Init()
        {
            _words = await _wordService.SelectRandomWords(allowUntranslated: false);
            _result.WordsCount = _words.Count;
        }

        public ICommand StartGame => new Command(async (e) => {
            await SelectNextWord();
            ShowStartScreen = false;
            ShowGameScreen = true;
            _watch = Stopwatch.StartNew();
        });

        public ICommand SubmitTranslation => new Command(async () => {
            _result.Attempts += 1;
            var isCorrect = string.Equals(ExpectedTranslation, Translation, StringComparison.InvariantCultureIgnoreCase);
            if (isCorrect)
            {
                TryAgain = false;
                ShowTranslation = false;
                Translation = "";
                _words.Remove(SelectedWord);
                await SelectNextWord();
            }
            else
            {
                ShowTranslation = TryAgain;
                TryAgain = true;
            }
        });

        private async Task SelectNextWord()
        {
            if (_words.Count == 0)
            {
                _watch.Stop();
                _result.ElapsedTime = _watch.ElapsedMilliseconds / 100;
                var page = _navigation.NavigationStack.Last();
                await _navigation.PushAsync(new Views.GameResult(_result));
                _navigation.RemovePage(page);
                await _gameResultService.SaveGameResult(_result);
                return;
            }

            SelectedWord = _words[0];
            if (RuToEn.Equals(SelectedDirection))
            {
                Value = SelectedWord.Translation;
                ExpectedTranslation = SelectedWord.Value;
            }
            else
            {
                Value = SelectedWord.Value;
                ExpectedTranslation = SelectedWord.Translation;
            }
        }
    }
}
