﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using InteractiveDictionary.Models;
using InteractiveDictionary.Services;
using InteractiveDictionary.Views;
using Xamarin.Forms;

namespace InteractiveDictionary.ViewModels
{
    class LatestSubmittedWordsViewModel : INotifyPropertyChanged
    {
        private readonly WordService _wordService = WordService.Instance;
        private readonly INavigation _navigation;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Word> Words { get; set; }

        public LatestSubmittedWordsViewModel(INavigation navigation)
        {
            _navigation = navigation;
            Init();
        }

        public async Task Init()
        {
            Words = new ObservableCollection<Word>(await _wordService.GetLatestSubmitted());
        }

        public ICommand SelectWord => new Command(async e =>
        {
            if (!(e is ItemTappedEventArgs itemTappedEvent)) return;
            await _navigation.PushAsync(new SubmittedWord(itemTappedEvent.Item as Word));
        });

        public ICommand OnWordDelete => new Command(async e =>
        {
            var word = (Word)e;
            await _wordService.DeleteWord(word);
            Words.Remove(word);
        });
    }
}
