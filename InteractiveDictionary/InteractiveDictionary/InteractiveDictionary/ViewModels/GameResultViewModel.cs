﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using InteractiveDictionary.Models;
using InteractiveDictionary.Services;
using Xamarin.Forms;

namespace InteractiveDictionary.ViewModels
{
    class GameResultViewModel : INotifyPropertyChanged
    {
        private readonly WordService _wordService = WordService.Instance;
        private readonly INavigation _navigation;

        public event PropertyChangedEventHandler PropertyChanged;

        public GameResult Result { get; set; }
        public string EllapsedTimeStr { get; set; }
        public string ResultMessage { get; set; }
        public Color ResultColor { get; set; }

        public GameResultViewModel(INavigation navigation, GameResult result)
        {
            _navigation = navigation;
            Result = result;

            var elapsedMin = result.ElapsedTime / 60;
            var elapsedSec = result.ElapsedTime % 60;
            var elapsedMinStr = elapsedMin > 0 ? $"{elapsedMin} min " : string.Empty;
            EllapsedTimeStr = $"{elapsedMinStr}{elapsedSec} sec";

            if (result.Attempts <= result.WordsCount + 1)
            {
                ResultMessage = "Well Done!";
                ResultColor = Color.Green;
            }
            else
            {
                ResultMessage = "I know you can do better!";
                ResultColor = Color.OrangeRed;
            }
        }
    }
}
