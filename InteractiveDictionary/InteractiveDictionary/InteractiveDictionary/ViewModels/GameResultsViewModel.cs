﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using InteractiveDictionary.Models;
using InteractiveDictionary.Services;
using Xamarin.Forms;

namespace InteractiveDictionary.ViewModels
{
    class GameResultsViewModel : INotifyPropertyChanged
    {
        private readonly GameResultService _gameResultService = GameResultService.Instance;
        private readonly INavigation _navigation;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<GameResult> GameResults { get; set; }

        public GameResultsViewModel(INavigation navigation)
        {
            _navigation = navigation;
            Init();
        }

        public async Task Init()
        {
            GameResults = new ObservableCollection<GameResult>(await _gameResultService.GetGameResults());
        }

        public ICommand SelectGameResult => new Command(async e =>
        {
            if (!(e is ItemTappedEventArgs itemTappedEvent)) return;
            await _navigation.PushAsync(new Views.GameResult(itemTappedEvent.Item as GameResult));
        });
    }
}
