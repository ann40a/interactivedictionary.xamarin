﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace InteractiveDictionary.Views
{
    public class MenuListView : ListView
    {
        public MenuListView()
        {
            List<MenuItem> data = new MenuListData();
            ItemsSource = data;
            VerticalOptions = LayoutOptions.FillAndExpand;
            BackgroundColor = Color.Transparent;

            var cell = new DataTemplate(typeof(TextCell));
            cell.SetBinding(TextCell.TextProperty, "Title");
            cell.SetValue(TextCell.TextColorProperty, Color.FromHex("E0E0E0"));

            ItemTemplate = cell;
            SelectedItem = data[0];
        }
    }
}
