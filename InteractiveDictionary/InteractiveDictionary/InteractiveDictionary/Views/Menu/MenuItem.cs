﻿using System;

namespace InteractiveDictionary.Views
{
    public class MenuItem
    {
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}
