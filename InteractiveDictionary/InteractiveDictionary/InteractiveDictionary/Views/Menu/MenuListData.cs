﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InteractiveDictionary.Views
{
    public class MenuListData : List<MenuItem>
    {
        public MenuListData()
        {
            Add(new MenuItem()
            {
                Title = "Submit Word",
                TargetType = typeof(SubmitNewWord)
            });

            Add(new MenuItem()
            {
                Title = "Popular Words",
                TargetType = typeof(LatestSubmittedWords)
            });

            Add(new MenuItem()
            {
                Title = "Find By Ear Game",
                TargetType = typeof(FindWordByEarGame)
            });

            Add(new MenuItem()
            {
                Title = "Translation Game",
                TargetType = typeof(TranslationGame)
            });

            Add(new MenuItem()
            {
                Title = "Game Results",
                TargetType = typeof(GameResults)
            });

            Add(new MenuItem()
            {
                Title = "About Me",
                TargetType = typeof(AboutMe)
            });
        }
    }
}
