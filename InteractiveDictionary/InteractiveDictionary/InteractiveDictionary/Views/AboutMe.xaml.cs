﻿using InteractiveDictionary.ViewModels;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace InteractiveDictionary.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutMe : ContentPage
    {
        public AboutMe()
        {
            InitializeComponent();
            BindingContext = new AboutMeViewModel();
            InitMapPins();
        }

        private async Task InitMapPins() {
            var position = new Position(53.863258, 27.595072);
            // var locator = CrossGeolocator.Current;
            // var position = await locator.GetPositionAsync(5000);
            MyLocationMap.Pins.Add(new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Developer Address",
                Address = "Belarus, Minsk, Plehanova 56/2",
            });
            MyLocationMap.MoveToRegion(
                MapSpan.FromCenterAndRadius(position, Distance.FromMiles(1.0))
            );
        }
    }
}