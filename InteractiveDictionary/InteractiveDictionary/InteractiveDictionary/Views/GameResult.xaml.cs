﻿using InteractiveDictionary.Models;
using InteractiveDictionary.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InteractiveDictionary.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GameResult : ContentPage
	{
		public GameResult(Models.GameResult result)
		{
			InitializeComponent();
		    BindingContext = new GameResultViewModel(Navigation, result);
        }
	}
}