﻿using InteractiveDictionary.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InteractiveDictionary.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TranslationGame : ContentPage
	{
		public TranslationGame()
		{
			InitializeComponent();
		    BindingContext = new TranslationGameViewModel(Navigation);
        }
	}
}