﻿using InteractiveDictionary.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InteractiveDictionary.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LatestSubmittedWords : ContentPage
	{
		public LatestSubmittedWords()
		{
            InitializeComponent();
		    BindingContext = new LatestSubmittedWordsViewModel(Navigation);
        }
	}
}