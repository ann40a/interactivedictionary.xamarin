﻿using InteractiveDictionary.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InteractiveDictionary.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SubmitNewWord : ContentPage
	{
		public SubmitNewWord()
		{
			InitializeComponent();
		    BindingContext = new SubmitNewWordViewModel(Navigation);
        }
	}
}