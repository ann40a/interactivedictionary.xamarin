﻿using InteractiveDictionary.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InteractiveDictionary.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FindWordByEarGame : ContentPage
	{
		public FindWordByEarGame()
		{
			InitializeComponent();
            BindingContext = new FindWordByEarGameViewModel(Navigation);
        }
	}
}
