﻿using InteractiveDictionary.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InteractiveDictionary.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GameResults : ContentPage
	{
		public GameResults()
		{
            InitializeComponent();
		    BindingContext = new GameResultsViewModel(Navigation);
        }
	}
}
