﻿using InteractiveDictionary.Models;
using InteractiveDictionary.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InteractiveDictionary.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SubmittedWord : ContentPage
	{
		public SubmittedWord (Word word)
		{
			InitializeComponent();
		    BindingContext = new SubmittedWordViewModel(Navigation, word);
        }
	}
}