﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using InteractiveDictionary.Models;
using SQLite;

namespace InteractiveDictionary
{
    public class InteractiveDictionaryDatabase
    {
        private readonly SQLiteAsyncConnection _database;

        public InteractiveDictionaryDatabase(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Word>().Wait();
            _database.CreateTableAsync<GameResult>().Wait();
        }

        public Task<List<T>> GetAsync<T>() where T : new()
        {
            return _database.Table<T>().ToListAsync();
        }

        public Task<List<T>> GetWhereAsync<T>(Expression<Func<T, bool>> predExpr) where T : new()
        {
            return _database.Table<T>().Where(predExpr).ToListAsync();
        }

        public Task<T> GetFirstWhereAsync<T>(Expression<Func<T, bool>> predExpr) where T : new()
        {
            return _database.Table<T>().Where(predExpr).FirstOrDefaultAsync();
        }

        public Task<List<T>> GetAsync<T>(Func<AsyncTableQuery<T>, AsyncTableQuery<T>> queryExpr) where T : new()
        {
            return queryExpr(_database.Table<T>()).ToListAsync();
        }

        public async Task CreateAsync<T>(T item)
        {
            await _database.InsertAsync(item);
        }

        public async Task UpdateAsync<T>(T item)
        {
            await _database.UpdateAsync(item);
        }

        public async Task DeleteAsync<T>(T item)
        {
            await _database.DeleteAsync(item);
        }

        public Task<List<T>> QueryAsync<T>(string query) where T : new()
        {
            return _database.QueryAsync<T>(query);
        }
    }
}
