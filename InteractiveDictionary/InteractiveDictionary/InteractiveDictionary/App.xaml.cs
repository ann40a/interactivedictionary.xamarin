﻿using InteractiveDictionary.Services;
using InteractiveDictionary.Views;
using Xamarin.Forms;

namespace InteractiveDictionary
{
	public partial class App : Application
	{
        private static InteractiveDictionaryDatabase _database;

	    public static InteractiveDictionaryDatabase Database
	    {
	        get
	        {
	            if (_database == null)
	            {
	                var dbPath = DependencyService.Get<IFileHelper>().GetLocalFilePath("TodoSQLite.db3");
                    _database = new InteractiveDictionaryDatabase(dbPath);
                }
	            return _database;
	        }
	    }

	    public App ()
		{
			InitializeComponent();
            MainPage = new RootPage();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
