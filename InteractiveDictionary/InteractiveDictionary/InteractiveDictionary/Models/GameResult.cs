﻿using System.ComponentModel;
using SQLite;

namespace InteractiveDictionary.Models
{
    public class GameResult : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public GameType GameType { get; set; }
        public int WordsCount { get; set; }
        public int Attempts { get; set; }
        public long ElapsedTime { get; set; }

        [Ignore]
        public string GameName {
            get {
                switch (GameType)
                {
                    case GameType.FindByEar:
                        return "Find By Ear";

                    case GameType.Translation:
                        return "Translation";
                }

                return GameType.ToString();
            }
        }
    }

    public enum GameType {
        FindByEar, Translation,
    }
}
