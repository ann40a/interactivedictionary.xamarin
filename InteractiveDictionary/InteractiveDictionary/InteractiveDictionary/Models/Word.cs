﻿using System.ComponentModel;
using SQLite;

namespace InteractiveDictionary.Models
{
    public class Word : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Value { get; set; }
        public string Translation { get; set; }
        public int Count { get; set; }
    }
}
