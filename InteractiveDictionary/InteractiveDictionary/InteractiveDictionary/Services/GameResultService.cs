﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InteractiveDictionary.Models;

namespace InteractiveDictionary.Services
{
    class GameResultService
    {
        public static GameResultService Instance = new GameResultService();

        public async Task<IEnumerable<GameResult>> GetGameResults()
        {
            return await App.Database.GetAsync<GameResult>(query => query
                .OrderByDescending(gr => gr.Id)
            );
        }

        public async Task SaveGameResult(GameResult result)
        {
            await App.Database.CreateAsync(result);
        }
    }
}
