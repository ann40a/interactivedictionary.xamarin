﻿namespace InteractiveDictionary.Services
{
    interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
