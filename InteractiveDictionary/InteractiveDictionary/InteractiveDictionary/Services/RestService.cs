﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace InteractiveDictionary.Services
{
    class RestService
    {
        public static RestService Instance = new RestService();

        private readonly HttpClient _client;

        public RestService()
        {
            _client = new HttpClient();
        }

        public async Task<T> Get<T>(string url)
        {
            var response = await _client.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException();
            }

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(content);
        }

        public async Task<string> Translate(string phrase)
        {
            var url = $"https://glosbe.com/gapi/translate?from=eng&dest=rus&format=json&phrase={phrase}";
            var result = await Get<TranslationResult>(url);
            return result.tuc?.First(p => p.phrase.language == "ru")?.phrase.text;
        }
    }

    class TranslationResult
    {
        public ICollection<TranslationItem> tuc { get; set; }
    }

    class TranslationItem
    {
        public TranslationPhrase phrase { get; set; }
    }

    class TranslationPhrase
    {
        public string text { get; set; }
        public string language { get; set; }
    }
}
