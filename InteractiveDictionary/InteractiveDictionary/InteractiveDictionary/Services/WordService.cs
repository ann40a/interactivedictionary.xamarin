﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InteractiveDictionary.Models;

namespace InteractiveDictionary.Services
{
    class WordService
    {
        private static readonly string defaultTranslation = "Unknown";

        public static WordService Instance = new WordService();

        private readonly RestService _restService = RestService.Instance;

        public async Task<IEnumerable<Word>> GetLatestSubmitted()
        {
            return await App.Database.GetAsync<Word>(query => query
                .OrderByDescending(w => w.Count)
                .Take(10)
            );
        }

        public async Task<Word> SubmitWord(string wordValue)
        {
            var word = await GetIfExists(wordValue);
            if (word == null)
            {
                var translation = await Translate(wordValue);
                word = new Word
                {
                    Value = wordValue,
                    Translation = translation,
                    Count = 1,
                };
                await App.Database.CreateAsync(word);
            }
            else
            {
                word.Count += 1;
                await UpdateWord(word);
            }

            return word;
        }

        public async Task UpdateWord(Word word)
        {
            await App.Database.UpdateAsync(word);
        }

        public async Task DeleteWord(Word word)
        {
            await App.Database.DeleteAsync(word);
        }

        public Task<List<Word>> SelectRandomWords(bool allowUntranslated = true, int limit = 3)
        {
            var where = allowUntranslated
                ? string.Empty
                : $"WHERE translation != '{defaultTranslation}'";
            return App.Database.QueryAsync<Word>($@"

SELECT Id, Value, Translation, Count
FROM (
    SELECT RANDOM() AS rand, Id, Value, Translation, Count
    FROM Word
    {where}
    ORDER BY rand
    LIMIT {limit}
) t

            ");
        }

        private Task<Word> GetIfExists(string wordValue)
        {
            return App.Database.GetFirstWhereAsync<Word>(w => wordValue.Equals(w.Value));
        }

        private async Task<string> Translate(string word)
        {
            try
            {
                return await _restService.Translate(word) ?? defaultTranslation;
            }
            catch (Exception) {
                return defaultTranslation;
            }
        }
    }
}
