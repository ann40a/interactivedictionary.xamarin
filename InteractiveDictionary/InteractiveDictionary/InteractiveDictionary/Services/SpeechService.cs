﻿using System.Threading.Tasks;
using Plugin.TextToSpeech;
using Plugin.TextToSpeech.Abstractions;

namespace InteractiveDictionary.Services
{
    class SpeechService
    {
        private static readonly CrossLocale EnglishLocale = new CrossLocale
        {
            Language = "en",
            Country = "US"
        };

        public static SpeechService Instance = new SpeechService();

        public async Task SpeakInEnglish(string phrase)
        {
            await CrossTextToSpeech.Current.Speak(phrase, EnglishLocale);
        }
    }
}
