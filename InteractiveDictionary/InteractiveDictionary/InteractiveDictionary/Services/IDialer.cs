﻿namespace InteractiveDictionary.Services
{
    interface IDialer
    {
        bool Dial(string number);
    }
}
