﻿using System;
using System.IO;
using InteractiveDictionary.Droid.Services;
using InteractiveDictionary.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]
namespace InteractiveDictionary.Droid.Services
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}