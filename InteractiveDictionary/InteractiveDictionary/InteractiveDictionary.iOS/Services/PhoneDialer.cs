﻿using Foundation;
using InteractiveDictionary.iOS.Services;
using UIKit;
using Xamarin.Forms;
using InteractiveDictionary.Services;

[assembly: Dependency(typeof(PhoneDialer))]
namespace InteractiveDictionary.iOS.Services
{
    class PhoneDialer : IDialer
    {
        public bool Dial(string number)
        {
            return UIApplication.SharedApplication.OpenUrl(
                new NSUrl("tel:" + number));
        }
    }
}